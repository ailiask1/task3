import java.text.DateFormatSymbols;
import java.util.Scanner;

public class Main {

    public boolean isNumeric(String inputValue) {
        if (inputValue == null) {
            return false;
        }
        try {
            int number = Integer.parseInt(inputValue);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean isStartedByZero(String number) {
        return number.startsWith("0");
    }

    public String isLeapYear(int year){
        return ((year % 100 == 0 && year % 400 == 0) || (year % 4 == 0 && year % 100 != 0) ? "leap year" : "normal year");
    }

    public String getMonth(int monthNumber) {
        return new DateFormatSymbols().getMonths()[monthNumber - 1];
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Main main = new Main();
        System.out.println("Please enter the year and month separated by a space on one line:");
        boolean inputValidation = false;
        String inputString = "";
        while (!inputValidation) {
            inputString = input.nextLine();
            if (inputString.contains("[a-zA-Z]+")){
                System.out.println("You enter letters. Please re-enter the values:");
                continue;
            }
            String[] yearAndMonth = inputString.split(" ");
            if (yearAndMonth.length != 2){
                System.out.println("Your entered values are incorrect. Please re-enter the values:");
            }
            else if (!main.isNumeric(yearAndMonth[0]) || !main.isNumeric(yearAndMonth[1])){
                System.out.println("You should enter only digits. Please re-enter the values:");
            }
            else if (main.isStartedByZero(yearAndMonth[0]) || main.isStartedByZero(yearAndMonth[1])){
                System.out.println("Number can't be started with the 0 digit. Please re-enter the values:");
            }
            else if (Integer.parseInt(yearAndMonth[0]) < 0 || Integer.parseInt(yearAndMonth[1]) <= 0){
                System.out.println("Years and months can't be negative. Please re-enter the values:");
            }
            else if (Integer.parseInt(yearAndMonth[1]) > 12){
                System.out.println("Month cannot be higher 12. Please re-enter the values:");
            }
            else {
                System.out.println(yearAndMonth[0] + " " + main.getMonth(Integer.parseInt(yearAndMonth[1])) + " - " + main.isLeapYear(Integer.parseInt(yearAndMonth[0])));
                inputValidation = true;
            }
        }
    }
}
